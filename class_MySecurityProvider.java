import java.security.Provider;
import java.security.Security;
import java.util.Enumeration;


public class MySecurityProvider extends Provider {
    public MySecurityProvider() {
        super("MySecurityProvider", 1.0, "My Security Provider Description");
        put("SecureRandom.MyAlgorithm", "com.example.MySecureRandom");
        put("MessageDigest.MyAlgorithm", "com.example.MyMessageDigest");
    }


    public void clear() {


    }


    public synchronized Enumeration<Object> elements() {


        return keys();
    }


    public static void main(String[] args) {
        Provider provider = new MySecurityProvider();
        Security.addProvider(provider);


        System.out.println("Provider: " + provider);
        System.out.println("Provider Name: " + provider.getName());
        System.out.println("Provider Version: " + provider.getVersion());
        System.out.println("Provider Info: " + provider.getInfo());


        Enumeration<Object> elements = provider.elements();
        while (elements.hasMoreElements()) {
            System.out.println(elements.nextElement());
        }
    }
}
